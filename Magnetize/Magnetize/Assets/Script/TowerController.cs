﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    private PlayerController playerCont;
    public TowerClick towerClick;

    private void Start()
    {
        playerCont = GameObject.Find("Player").GetComponent<PlayerController>();
        //towerClick = GameObject.Find("towerBtn").GetComponent<TowerClick>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (towerClick.mouseDown)
        {
            playerCont.hookedToTower();
        }
        else if (!towerClick.mouseDown)
        {
            playerCont.unHookedToTower();
            //Debug.Log("sssss");
        }
    }

   
}
