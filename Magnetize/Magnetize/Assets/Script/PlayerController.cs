﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2D;

    public float moveSpeed = 5f;
    public float pullForce = 100f;
    public float rotateSpeed = 360f;

    private GameObject closestTower;
    private GameObject hookedTower;

    private bool isPulled = false;

    private AudioSource myAudio;
    private bool isCrashed = false;

    private Vector3 startPosition;

    private UIControllerScript uiControl;

    void Start()
    {
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();

        uiControl = GameObject.Find("Canvas").GetComponent<UIControllerScript>();

        myAudio = this.gameObject.GetComponent<AudioSource>();

        startPosition = this.transform.position;
    }


    void Update()
    {
        //rb2D.velocity = -transform.up * moveSpeed;
        Debug.Log(rb2D.angularVelocity);

        

        if (Input.GetKeyUp(KeyCode.Z))
        {
            
        }

        if (isCrashed)
        {
            if (!myAudio.isPlaying)
            {
                restartPosition();
            }
        }
        else
        {
            rb2D.velocity = -transform.up * moveSpeed;
            //rb2D.angularVelocity = 0f;
        }
    }

    public void hookedToTower()
    {
        if (!isPulled)
        {
            if (closestTower != null && hookedTower == null)
            {
                hookedTower = closestTower;
            }
            if (hookedTower)
            {
                float distance = Vector2.Distance(transform.position, hookedTower.transform.position);

                Vector3 pullDirection = (hookedTower.transform.position - transform.position).normalized;
                float newPullForce = Mathf.Clamp(pullForce / distance, 20, 50);
                rb2D.AddForce(pullDirection * newPullForce);

                rb2D.angularVelocity = -rotateSpeed / distance;
                isPulled = true;
            }
        }
    }

    public void unHookedToTower()
    {
        rb2D.angularVelocity = 0;
        isPulled = false;
        hookedTower = null;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Tower" )
        {
            if (!isCrashed)
            {
                myAudio.Play();
                rb2D.velocity = new Vector3(0f, 0f, 0f);
                rb2D.angularVelocity = 0f;
                isCrashed = true;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Goal")
        {
            Debug.Log("Level Clear");
            uiControl.endGame();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Tower")
        {
            closestTower = collision.gameObject;
            collision.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (isPulled) return;

        if (collision.gameObject.tag == "Tower")
        {
            closestTower = null;
            hookedTower = null;
            collision.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    public void restartPosition()
    {
        //Vector3 startPosition = default;
        this.transform.position = startPosition;

        this.transform.rotation = Quaternion.Euler(0f, 0f, 90f);

        isCrashed = false;

        if (closestTower)
        {
            closestTower.GetComponent<SpriteRenderer>().color = Color.white;
            closestTower = null;
        }

    }
}
